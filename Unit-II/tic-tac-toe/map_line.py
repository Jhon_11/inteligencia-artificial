def map_line(n, start1, stop1, start2, stop2):
    """
        Reference: https://github.com/processing/p5.js/blob/main/src/math/calculation.js
    """
    
    return (n - start1) / (stop1 - start1) * (stop2 - start2) + start2
