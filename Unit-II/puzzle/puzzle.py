class Puzzle:
    """
        Generalized implementation of an 8-Puzzle.
    """
    

    def __init__(self, board=None, size=3):

        self.size = size
        self.length = self.size * self.size
        if board:
            self.board = board
            self.cero_index = self.board.index(0)
        else:
            self.cero_index = 0
            self.board = [x for x in range(self.length)]


    def get_board(self): return self.board


    def __eq__(self, board): return self.board == board


    def __repr__(self): 
        board = [
                '\t'.join(map(str, self.board[i:i + self.size]))
                for i in range(0, len(self.board), self.size)
        ]
        return '\n'.join(board)


    def __str__(self): 
        board = [
                '\t'.join(map(str, self.board[i:i + self.size]))
                for i in range(0, len(self.board), self.size)
        ]
        return '\n'.join(board)


    def _swap(self, i, j):

        self.board[i], self.board[i + j] = self.board[i + j], self.board[i]

    def move(self, direction, silent=True):
        """
            Make IN-PLACE move in the given direction valid.
            If silent=True value is given return a list of the final state of the board.
            Return None if direction is not valid.
        """
        
        
        rules = ['up', 'down', 'left', 'right']
        result = None

        if direction in rules:
            if direction == 'up' and self.cero_index - self.size >= 0:
                self._swap(self.cero_index, -self.size)
                self.cero_index -= 3
                result = self.board
                
            elif direction == 'down' and self.cero_index + self.size < self.length:
                self._swap(self.cero_index, self.size)
                self.cero_index += self.size
                result = self.board
                
            elif (direction == 'left'
                    and self.cero_index - 1 >= 0
                    and self.cero_index // self.size == (self.cero_index - 1) //
                    self.size):
                self._swap(self.cero_index, -1)
                self.cero_index -= 1
                result = self.board

            elif (direction == 'right'
                    and self.cero_index + 1 >= 0
                    and self.cero_index // self.size == (self.cero_index + 1) //
                    self.size):
                self._swap(self.cero_index, 1)
                self.cero_index += 1
                result = self.board
        if not silent:
            return result
