%Facts
teach(raul, data_structures).
teach(cristian, data_bases).
teach(cristian, programming).
teach(joel, ia).
teach(joel, ml).
teach(joel, poo).

student(diego, ia).
student(nicol, ia).
student(alvaro, ia).
student(cba, ia).
student(felipe, ia).
student(felipe, ml).

%Rules
is_student(P, A) :-
    teach(P, C),
    student(A, C).
